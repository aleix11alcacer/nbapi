from nbapi.endpoints.__endpoint import Endpoint


class CommonTeamRoster(Endpoint):

    def __init__(self, **kargs):
        super(CommonTeamRoster, self).__init__(**kargs)
