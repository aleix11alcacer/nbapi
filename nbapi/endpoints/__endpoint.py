from nbapi.helpers.request import send_request
from nbapi.helpers.dataframe import create_dataframe


class Endpoint:

    def __init__(self, **kargs):
        self.__url = 'http://stats.nba.com/stats/' + self.__class__.__name__.lower()
        self.__response = send_request(self.__url, **kargs)
        self.__options = [self.__response['resultSets'][i]['name'] for i in range(len(self.__response['resultSets']))]

    @property
    def type(self):
        return self.__response['resource']

    @property
    def params(self):
        params = self.__response['parameters']
        return params

    @property
    def options(self):
        return self.__options

    def __getitem__(self, item):
        index = self.__options.index(item)
        return create_dataframe(self.__response, index)


def get_params(endpoint):
    url = 'http://stats.nba.com/stats/' + endpoint.__name__.lower()

    # Obtain minimal params needed for the request

    response = requests.get(
        url,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )
    content = response.content.decode('utf-8')
    keys = [parse_key(key) for key in content.split("; ")]

    minimal_defaults = {}
    for key in keys:
        minimal_defaults[key] = defaults[key]

    # Obtain all params needed for the request

    response = requests.get(
        url,
        params=minimal_defaults,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )

    complete_defaults = response.json()['parameters']

    return list(complete_defaults.keys())
