from nbapi.endpoints.__endpoint import Endpoint


class TeamInfoCommon(Endpoint):

    def __init__(self, **kargs):
        super(TeamInfoCommon, self).__init__(**kargs)
