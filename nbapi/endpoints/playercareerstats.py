from nbapi.endpoints.__endpoint import Endpoint


class PlayerCareerStats(Endpoint):

    def __init__(self, **kargs):
        super(PlayerCareerStats, self).__init__(**kargs)
