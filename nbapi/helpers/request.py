from .defaults import defaults
import requests


def parse_key(key):
    key = key.replace('.', '')
    key = key.replace('The ', '')
    key = key.replace('property ', '')
    key = key.replace(' is required', '')

    return key


def get_params(endpoint):
    url = 'http://stats.nba.com/stats/' + endpoint.__name__.lower()

    # Obtain minimal params needed for the request

    response = requests.get(
        url,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )
    content = response.content.decode('utf-8')
    keys = [parse_key(key) for key in content.split("; ")]

    minimal_defaults = {}
    for key in keys:
        minimal_defaults[key] = defaults[key]

    # Obtain all params needed for the request

    response = requests.get(
        url,
        params=minimal_defaults,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )

    complete_defaults = response.json()['parameters']

    return list(complete_defaults.keys())


def send_request(url, **kargs):

    # Obtain minimal params needed for the request

    response = requests.get(
        url,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )
    content = response.content.decode('utf-8')
    keys = [parse_key(key) for key in content.split("; ")]

    minimal_defaults = {}
    for key in keys:
        minimal_defaults[key] = defaults[key]

    # Obtain all params needed for the request

    response = requests.get(
        url,
        params=minimal_defaults,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )

    complete_defaults = response.json()['parameters']

    # Make request with the params passed by the user

    for key, value in kargs.items():
        if key in complete_defaults:
            complete_defaults[key] = value

    response = requests.get(
        url,
        params=complete_defaults,
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        timeout=100
    )

    if response.status_code != 200:
        raise ValueError
    else:
        return response.json()
