import pandas as pd


def create_dataframe(response, index):
    res = response['resultSets'][index]
    df = pd.DataFrame(res['rowSet'], columns=res['headers'])
    df.name = res['name']
    return df.infer_objects()
