# NBAPI

Minimal API to get NBA data from [stats.nba.com](stats.nba.com).  Still on development.

## Installation

```
python setup.py install
```

### Develop mode

```
python setup.py develop
```

```
python setup.py develop --uninstall
```