from nbapi.endpoints import CommonAllPlayers, PlayerCareerStats
from nbapi import get_params


params = get_params(CommonAllPlayers)
print(params)

players = CommonAllPlayers()

df = players['CommonAllPlayers']

lebron = df[df['DISPLAY_FIRST_LAST'].str.contains("LeBron")]

lebron_id = lebron['PERSON_ID']

stats = PlayerCareerStats(PlayerID=lebron_id)

data = stats['SeasonTotalsRegularSeason']

print(data[['SEASON_ID', 'TEAM_ABBREVIATION', 'PTS', 'AST', 'REB', 'FG_PCT']])
