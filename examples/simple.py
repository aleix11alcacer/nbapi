from nbapi.endpoints import CommonTeamRoster, PlayerCareerStats


westbrook = PlayerCareerStats(PlayerID=201566, PerMode='PerGame')

print(f'Request type: {westbrook.type}')
print(f'Request params: {westbrook.params}')
print(f'Request options: {westbrook.options}')

df = westbrook['SeasonTotalsRegularSeason']

print(df[['SEASON_ID', 'TEAM_ABBREVIATION', 'PTS', 'AST', 'REB']])

phila = CommonTeamRoster(TeamID=1610612755)

print(f'Request type: {phila.type}')
print(f'Request params: {phila.params}')
print(f'Request options: {phila.options}')

df = phila['Coaches']

print(df[['COACH_NAME', 'COACH_ID']])
